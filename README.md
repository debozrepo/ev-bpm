# ev-bpm

## How To Run
Main Entry point is `ev-bpm-service`. use following command to execute file.
`chmod 755 launch.sh`
`./launch.sh clean install`

## Verify installation
go to browser. check following url:
`http://localhost:8090/ev-bpm/rest/server`
This should provide following result
`<response type="SUCCESS" msg="Kie Server info">
<kie-server-info>
<capabilities>KieServer</capabilities>
<capabilities>BRM</capabilities>
<capabilities>BPM</capabilities>
<capabilities>CaseMgmt</capabilities>
<capabilities>BPM-UI</capabilities>
<capabilities>DMN</capabilities>
<location>http://localhost:8090/ev-bpm/rest/server</location>
<messages>
<content>
Server KieServerInfo{serverId='ev-bpm-service', version='7.31.0.Final', name='ev-bpm-service', location='http://localhost:8090/ev-bpm/rest/server', capabilities=[KieServer, BRM, BPM, CaseMgmt, BPM-UI, DMN]', messages=null', mode=DEVELOPMENT}started successfully at Sat Jan 18 23:55:04 CST 2020
</content>
<severity>INFO</severity>
<timestamp>2020-01-18T23:55:04.579-06:00</timestamp>
</messages>
<mode>DEVELOPMENT</mode>
<name>ev-bpm-service</name>
<id>ev-bpm-service</id>
<version>7.31.0.Final</version>
</kie-server-info>
</response>`